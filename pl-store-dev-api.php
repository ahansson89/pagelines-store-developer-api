<?php
/*
Plugin Name: PageLines Store Developer API
Plugin URI: http://pagelines-developers.com/
Description: 
Author: Evan Mattson
Author URI: 
Version: 1.0
pagelines: true
*/

final class PLStoreDevAPI {

	const version = '1.0';

	private $creds;

	// Construct
	function __construct( $username = '', $apikey = '', $cache = true ) {

		$this->id  = strtolower(__CLASS__);
		$this->url = sprintf('%s/%s', WP_PLUGIN_URL, $this->id);
		$this->dir = sprintf('%s/%s', WP_PLUGIN_DIR, $this->id);

		$this->creds = array(
			'user'     => $username,
			'key'      => $apikey,
		);
		$this->mode     = ($this->creds['user'] && $this->creds['key']) ? 'authenticated' : 'public';
		$this->products = array();
		$this->cache    = $cache;

		$this->api = array(
			'dataurl'  => 'http://api.pagelines.com/devs',
			'version'  => '1.1',
			'endpoint' => ''
		);

		$this->actions();

		//plprint( $this->cache ? 'caching is ON' : 'caching is off', 'Store API Cache' );
	}

	function actions() {

		// Setup Products
		if ( 'authenticated' == $this->mode )
		add_action( 'init',						array(&$this, 'setup_products') );

		// Custom option
		add_action( 'pagelines_options_apikey', array(&$this, 'apikey_option'), 10, 2 );
	}

	public function request( $request, $args = array() ) {
		return $this->api_request( $request, $args );
	}

	/**
	 * Submits a request to the API
	 *
	 * @uses  $this->is_private_request() 	checks to see if the request requires validation
	 * @uses  $this->return_response() 		determines what to return on success
	 * 
	 * @param  string 	$request 	request slug
	 * @param  array  	$args    	request modifiers/options
	 * @return mixed 				array on success (see return_response method)
	 *                         		string html error messages on error
	 */
	private function api_request( $request, $args = array() ) {

		$d = array(
			'product' => '',
			'days' => ''
		);

		$args = wp_parse_args( $args, $d );

		// only extract keys defined in defaults
		extract( shortcode_atts( $d, $args ) );

		extract( $this->api );
		extract( $this->creds );

		switch ( $request ) {
			
			case 'developer-total':
				$endpoint = sprintf('all%s', $days ? ":$days" : '');
				break;
			
			case 'product-total':
				$endpoint = sprintf('sales:%s', $days ? "$product,$days" : $product);
				break;
			
			case 'list-products':
				$endpoint = 'list';
				break;

			case 'some-public-request':
				// nothing happening here yet
				break;
		}

		$cached = $this->get_cached_request( $request, $args );

		// use cached?
		if ( $this->cache && is_array( $cached ) ) {
			$args['cached'] = true;
			return $this->return_response( $cached, $args );
		}

		//	at this point the cached data has expired
		//	or nothing has been cached for this request yet
		//  make a new request to the api
		$api_request = "$dataurl/$version";
		$api_request .= $this->is_private_request( $request ) ? "/$user/$key" : '';
		$api_request .= "/$endpoint";

		// send the request
		$response = wp_remote_get( esc_url( $api_request ) );

		//plprint($api_request, 'new request');

		// check the response
		if ( is_wp_error( $response ) ) {

			$error = '<p><strong>Error</strong></p>';
			foreach ( $response->errors as $error )
				$error .= '<p>'.$error[0].'</p>';

			return $error;
		}
		elseif ( isset($response['response']['code']) && 200 === $response['response']['code'] ) {
			$this->cache_response( $request, $args, $response );
			return $this->return_response( $response, $args );
		}
	}

	private function get_cached_request( $request, $args ) {

		$key = $this->get_cached_key( $request, $args );

		$cached = $key ? get_transient( $key ) : false;

		//plprint( !is_array($cached) ? 'no cached data' : $cached, 'get cached '. $key);

		return $cached;
	}

	private function cache_response( $request, $args, $data ) {

		$key = $this->get_cached_key( $request, $args );

		if ( $key && $this->cache )
			set_transient( $key, $data, HOUR_IN_SECONDS*2 );
	}

	private function get_cached_key( $request, $args ) {

		$user = $this->creds['user'];

		if ( 'product-total' == $request && $args['product'] ) {

			$request .= "_{$args['product']}"; // append product slug
			
			if ( $args['days'] )
				$request .= "_{$args['days']}-days";
		}

		return "{$user}_$request";
	}

	private function return_response( $response, $args ) {

		if ( isset($args['full']) && $args['full'] )
			$r = $response;
		else {
			$r = json_decode( $response['body'], true );
			$r['date'] = $response['headers']['date'];
		}

		// tag the returned data as cached
		if ( isset($args['cached']) && $args['cached'] )
			$r['cached'] = true;

		return $r;
	}

	private function is_private_request( $request ) {
		$private = array(
			'developer-total',
			'product-total',
			'list-products'
		);
		return in_array( $request, $private );
	}

	/**
	 * Sets up the 'products' property with an array of user's product data in this format:
	 * array('product-slug' => array(
	 * 		'id'			=> 123,
	 * 		'slug'			=> 'some-product-slug',
	 * 		'name'			=> 'Real Product Name',
	 * 		'description'	=> 'blah blah blah',
	 * 		'lastmod'		=> 1359510914, // timestamp
	 * 		'price'			=> '19.95',
	 * 		'image'			=> 'http://api.pagelines.com/files/plugins/img/some-product-slug-thumb.png',
	 * 		'demo'			=> 'some demo url',
	 * 		'downloads'		=> 321
	 *   	)
	 * )
	 *
	 * @uses  'list-products' 	api request type
	 */
	function setup_products() {

		$user = $this->creds['user'];

		$vault = get_transient("{$user}_products");

		// check for cached
		if ( $this->cache && is_array($vault) && isset($vault['products']) ) {
			$this->products = $vault['products'];
		}
		else {

			// otherwise build/rebuild the product table!
			$r = $this->api_request( 'list-products', array('full' => 1) );

			$list = json_decode( $r['body'], true );
			
			$products = array();
			foreach ( $list as $p )
				$products[ $p['slug'] ] = $p;

			$vault = array(
				'age'      => $r['headers']['date'],
				'products' => $products,
			);
			
			if ( $this->cache )
				set_transient( "{$user}_products", $vault, HOUR_IN_SECONDS*2 );

			$this->products = $products;
		}

		//plprint($this->products, 'product table');
	}

	function validate_product( $slug ) {
		return array_key_exists( $slug, $this->products ) ? $slug : '';
	}

	/**
	 * Helper function for retreiving product info
	 * @param  string 	$slug 	product slug
	 * @param  string 	$key  	info key
	 * @return mixed
	 */
	function get_product_info( $slug, $key ) {
		return isset( $this->products[ $slug ][ $key ] ) ? $this->products[ $slug ][ $key ] : '';
	}

	/**
	 * Custom Option
	 * Works the same as 'text' but displays as input[type=password]
	 */
	function apikey_option( $oid, $o ) {

		echo OptEngine::input_label($o['input_id'], $o['inputlabel']);
		echo OptEngine::input_text($o['input_id'], $o['input_name'], pl_html($o['val']), 'regular-text', 'password', '', $o['placeholder'] );		
	}

} // PLStoreDevAPI